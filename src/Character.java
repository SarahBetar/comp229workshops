import java.awt.*;
import java.util.Optional;

import bos.GamePiece;
import bos.RelativeMove;

public abstract class Character implements GamePiece<Cell> {
    Optional<Color> display;
    Cell location;
    MoveBehaviour moveBehaviour;

    public Character(Cell location, MoveBehaviour moveBehaviour){
        this.location = location;
        this.display = Optional.empty();
        this.moveBehaviour = moveBehaviour;
    }

    public  void paint(Graphics g){
        if(display.isPresent()) {
            g.setColor(display.get());
            g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
        }
    }

    public void setLocationOf(Cell loc){
        this.location = loc;
    }

    public Cell getLocationOf(){
        return this.location;
    }

    public void setBehaviour(MoveBehaviour moveBehaviour){
        this.moveBehaviour = moveBehaviour;
    }

    public RelativeMove aiMove(Stage stage){
        return moveBehaviour.chooseMove(stage, this);
    }
}
