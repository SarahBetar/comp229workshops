import java.awt.*;
import java.util.Optional;

public class Sheep extends Character {

    public Sheep(Cell location, MoveBehaviour mb) {
        super(location, mb);
        display = Optional.of(Color.WHITE);
    }
}