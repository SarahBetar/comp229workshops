import bos.*;

import java.awt.*;
import java.util.Optional;

public class RabbitAdapter extends Character {
    private Rabbit rabbit = new Rabbit();

    public RabbitAdapter(Cell location) {
        super(location, null);
        display = Optional.of(Color.LIGHT_GRAY);
    }

    @Override
    public RelativeMove aiMove(Stage stage){
        switch(rabbit.nextMove()) {
            case 0: return new MoveLeft(stage.grid, this);
            case 1: return new MoveRight(stage.grid, this);
            case 2: return new MoveUp(stage.grid, this);
            case 3: return new MoveDown(stage.grid, this);
            default: return new NoMove(stage.grid, this);
        }
    }
}
