import java.awt.*;
import java.util.Optional;

public class Shepherd extends Character {

    private volatile static Shepherd uniqueInstance;

    public Shepherd(Cell location, MoveBehaviour mb) {
        super(location, mb);
        display = Optional.of(Color.GREEN);
    }

}