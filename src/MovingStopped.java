import bos.NoMove;
import bos.RelativeMove;

public class MovingStopped implements MoveBehaviour {
    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        return new NoMove(stage.grid, mover);
    }
}
