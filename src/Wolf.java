import java.awt.*;
import java.util.Optional;

public class Wolf extends Character {

    public Wolf(Cell location, MoveBehaviour mb) {
        super(location, mb);
        display = Optional.of(Color.RED);
    }

}