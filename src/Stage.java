import bos.Rabbit;
import java.awt.*;
import java.util.*;
import java.time.*;
import java.util.List;

public class Stage extends KeyObservable {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected Player player;
    protected RabbitAdapter rabbit;
    private List<Character> allCharacters;

    private volatile static Stage uniqueInstance;

    private Instant timeOfLastMove = Instant.now();

    private Stage() {
        grid = new Grid(10, 10);
        shepherd = new Shepherd(grid.getRandomCell(), new MovingStopped());
        sheep = new Sheep(grid.getRandomCell(), new MoveTowards(shepherd));
        wolf = new Wolf(grid.getRandomCell(), new MoveTowards(sheep));
        rabbit = new RabbitAdapter(grid.getRandomCell());

        player = new Player(grid.getRandomCell());
        this.register(player);

        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep);
        allCharacters.add(shepherd);
        allCharacters.add(wolf);
        allCharacters.add(rabbit);

    }

    public static Stage getInstance() {
            if(uniqueInstance == null) {
                synchronized (Stage.class) {
                    if(uniqueInstance == null)
                        uniqueInstance = new Stage();
                }
            }
            return uniqueInstance;
    }

    public void update() {
        if (!player.inMove()) {
            if (sheep.location == shepherd.location) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else {
                if (sheep.location.x == sheep.location.y) {
                    sheep.setBehaviour(new MovingStopped());
                    shepherd.setBehaviour(new MoveTowards(sheep));
                }
                allCharacters.forEach((c) -> c.aiMove(this).perform());
                player.startMove();
                timeOfLastMove = Instant.now();
            }
        }
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);
        rabbit.paint(g);
    }
}