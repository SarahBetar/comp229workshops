import bos.RelativeMove;

public interface MoveBehaviour {
    public RelativeMove chooseMove(Stage stage, Character mover);
}
